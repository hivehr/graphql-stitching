import { server as bindingsServer } from "./bindings/server";
import { server as stitchingsServer } from "./stitchings/server";

bindingsServer.start(
    {
        port: 9000
    },
    () => {
        console.log("Binding example available on http://localhost:9000");
    }
);

stitchingsServer.start(
    {
        port: 9001
    },
    () => {
        console.log("Stitching example available on http://localhost:9001");
    }
);
