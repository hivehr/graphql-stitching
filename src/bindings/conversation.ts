import { GraphQLResolveInfo } from "graphql";
import gql from "graphql-tag";
import { Context } from "../context";
import { hasRoles } from "../rules/hasRoles";
import { PromiseType } from "../types";

const conversationResolver = (parent: any, { id }: any, context: Context, info: GraphQLResolveInfo) =>
    context.models.conversation.getConversation(id, context.jwt.organizationId, info);

export const typeDefs = gql`
    enum ConversationRelationType {
        answer
        recognition
        suggestion
    }

    enum ConversationRelationSubject {
        author
        recipient
    }

    type ConversationRelation {
        id: String!
        relationId: String!
        relationType: ConversationRelationType!
        relationSubject: ConversationRelationSubject!
    }

    type Conversation {
        id: String!
        participants: [User]!
        relatesTo: ConversationRelation!
        tags: [String]!
        closedAt: String
        closedBy: String
        createdAt: String!
        updatedAt: String
    }

    extend type Query {
        conversation(id: String!): Conversation
    }
`;

export const resolvers = {
    Query: {
        conversation: conversationResolver
    },

    Conversation: {
        participants: (
            { participants }: PromiseType<typeof conversationResolver>,
            args: any,
            context: Context,
            info: GraphQLResolveInfo
        ) =>
            Promise.all(
                participants.map((id: string) => context.models.user.getUser(id, context.jwt.organizationId, info))
            )
    }
};

export const permissions = {
    Query: {
        conversation: hasRoles(["read"])
    }
};
