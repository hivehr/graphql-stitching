import gql from "graphql-tag";
import { mergeSchemas, makeExecutableSchema } from "graphql-tools";
import { GraphQLServer } from "graphql-yoga";
import { merge } from "lodash";
import { context } from "../context";
import * as conversation from "./conversation";
import * as user from "./user";
import { generateShield } from "../utils/generateShield";

const Query = gql`
    type Query {
        _empty: String
    }
`;

const Mutation = gql`
    type Mutation {
        _empty: String
    }
`;

const Subscription = gql`
    type Subscription {
        _empty: String
    }
`;

const typeDefs = [conversation.typeDefs, user.typeDefs];

const resolvers = merge({}, conversation.resolvers, user.resolvers);

const shield = generateShield(merge({}, conversation.permissions, user.permissions));

// We used mergeSchemas here to provide the resolvers, rather than passing them
// directly to the `makeExecutableSchema` call, as the latter does not allow the
// `fragment` property alongside resolvers, which we need in order to ensure certain
// properties are selected on parent fields, for use within child resolvers.
// TODO: This might be achievable in a nicer way? Probably built-in for all I know!
const schema = mergeSchemas({
    schemas: [
        makeExecutableSchema({
            typeDefs: [Query, Mutation, Subscription, ...typeDefs]
        })
    ],
    resolvers
});

export const server = new GraphQLServer({
    schema,
    context,
    middlewares: [shield]
});
