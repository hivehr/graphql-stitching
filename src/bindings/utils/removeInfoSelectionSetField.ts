import { GraphQLResolveInfo } from "graphql";
import { filter } from "lodash";
import immutable from "object-path-immutable";

export const removeInfoSelectionSetField = (info: GraphQLResolveInfo, fieldName: string): GraphQLResolveInfo =>
    immutable.update(info, ["fieldNodes", "0", "selectionSet", "selections"], (selections: any[]) =>
        filter(selections, item => item.name.value !== fieldName)
    ) as any;
