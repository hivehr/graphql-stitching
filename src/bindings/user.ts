import { GraphQLResolveInfo } from "graphql";
import { addFragmentToInfo } from "graphql-binding";
import gql from "graphql-tag";
import { Context } from "../context";
import { hasRoles } from "../rules/hasRoles";
import { PromiseType } from "../types";
import { removeInfoSelectionSetField } from "./utils/removeInfoSelectionSetField";

const userResolver = (parent: any, { id }: any, context: Context, info: GraphQLResolveInfo) =>
    context.models.user.getUser(id, context.jwt.organizationId, info);

export const typeDefs = gql`
    type User {
        id: String!
        username: String!
        email: String!
        conversations: [Conversation]!
        firstName: String!
        lastName: String!
    }

    extend type Query {
        user(id: String!): User
    }
`;

export const resolvers = {
    Query: {
        user: userResolver
    },

    User: {
        conversations: {
            fragment: `... on User { id }`,
            resolve: (
                { id }: PromiseType<typeof userResolver>,
                args: any,
                context: Context,
                info: GraphQLResolveInfo
            ) =>
                context.models.conversation.getUserConversations(
                    id,
                    context.jwt.organizationId,
                    addFragmentToInfo(
                        removeInfoSelectionSetField(info, "participants"),
                        `fragment ConversationParticipants on Conversation {
                            participants
                        }`
                    )
                )
        }
    }
};

export const permissions = {
    Query: {
        user: hasRoles(["read"])
    }
};
