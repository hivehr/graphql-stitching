import { ContextCallback } from "graphql-yoga/dist/types";
import { mapValues } from "lodash";
import { Models, models } from "./models/index";
import { decodeJwt, JWT } from "./utils/decodeJwt";
import { MapReturnTypes } from "./types";

export interface Context {
    jwt: JWT;
    models: MapReturnTypes<Models>;
}

export const context: ContextCallback = (req): Context => ({
    jwt: decodeJwt(req.request.headers.authorization),

    models: mapValues(models, factory => mapValues(factory, (resolver: any) => resolver(req))) as Context["models"]
});
