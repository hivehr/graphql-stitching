import { Context } from "../context";
import { rule } from "graphql-shield";

export const isAuthenticated = rule()((parent: any, args: any, context: Context, info) => context.jwt != null);
