import { Context } from "../context";
import { rule } from "graphql-shield";

export const hasRoles = (roles: string[]) =>
    rule()((parent: any, args: any, context: Context, info) => roles.every(role => context.jwt.roles.has(role)));
