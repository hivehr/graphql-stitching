import * as faker from "faker";

export interface JWT {
    userId: string;
    roles: Set<string>;
    organizationId: string;
}

// In real life , we'd decode these from the JWT passed in the Authorization Bearer token
export const decodeJwt = (jwt?: string) => {
    // if (jwt == null) {
    //     throw new Error("JWT not provided");
    // }

    return {
        userId: faker.random.uuid(),
        roles: new Set(["create", "read", "update", "delete"]),
        organizationId: faker.random.uuid()
    };
};
