import { shield } from "graphql-shield";
import { isAuthenticated } from "../rules/isAuthenticated";

const IS_PROD = process.env.NODE_ENV !== "production";

export const generateShield = (permissions: {}) => {
    return shield(permissions, {
        // Debugging stuff
        debug: IS_PROD,
        allowExternalErrors: IS_PROD,

        // Apply a default rule to all fields / types to ensure the user is authenticated. This
        // might just need to be `deny` instead if we want to allow certain fields to be accs
        fallbackRule: isAuthenticated,

        // Global fallback error if we don't handle authentication ourselves within the rule
        fallbackError: new Error("You are not authorized to view one or more of these fields / types.")
    });
};
