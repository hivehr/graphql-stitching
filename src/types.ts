export type ThenArg<T> = T extends Promise<infer U> ? U : T extends (...args: any[]) => Promise<infer U> ? U : T;

export type PromiseType<T> = T extends ((...args: any[]) => infer U) ? ThenArg<U> : T;

export type MapReturnTypes<O extends any> = { [K in keyof O]: ReturnType<O[K]> };
