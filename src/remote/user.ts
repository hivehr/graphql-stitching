import * as faker from "faker";
import { Binding } from "graphql-binding";
import { makeExecutableSchema } from "graphql-tools";
import gql from "graphql-tag";
import { randomArray } from "./utils/randomArray";

const getUser = (id = faker.random.uuid()) => ({
    id,
    username: faker.internet.userName(),
    email: faker.internet.email(),
    password: faker.random.uuid(),
    firstName: faker.name.firstName(),
    lastName: faker.name.lastName()
});

const typeDefs = gql`
    type User {
        id: String!
        username: String!
        email: String!
        password: String!
        firstName: String!
        lastName: String!
    }

    type Query {
        user(id: String!): User!
        users(organizationId: String!): [User]!
    }
`;

const resolvers = {
    Query: {
        user: (_: any, { id }: { id: string }) => getUser(id),
        users: () => randomArray(() => getUser(), 5, 0)
    }
};

const schema = makeExecutableSchema({
    typeDefs,
    resolvers: resolvers as any
});

export const binding = new Binding({
    schema
});
