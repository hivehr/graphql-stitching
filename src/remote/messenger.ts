import * as faker from "faker";
import { Binding } from "graphql-binding";
import { makeExecutableSchema } from "graphql-tools";
import gql from "graphql-tag";
import { randomArray } from "./utils/randomArray";

const getConversation = (id = faker.random.uuid()) => ({
    id,
    participants: randomArray(() => faker.random.uuid(), 5),
    relatesTo: {
        id: faker.random.number(),
        relationId: faker.random.number(),
        relationType: faker.random.arrayElement(["answer", "recognition", "suggestion"]),
        relationSubject: faker.random.arrayElement(["author", "recipient"])
    },
    tags: randomArray(() => faker.lorem.word(), 3),
    closedAt: faker.random.arrayElement([null, faker.date.past().toISOString()]),
    closedBy: faker.random.arrayElement([null, faker.name.firstName()]),
    createdAt: faker.date.past().toISOString(),
    updatedAt: faker.date.past().toISOString()
});

const typeDefs = gql`
    enum RelationType {
        answer
        recognition
        suggestion
    }

    enum RelationSubject {
        author
        recipient
    }

    type Relation {
        id: String!
        relationId: String!
        relationType: RelationType!
        relationSubject: RelationSubject!
    }

    type Conversation {
        id: String!
        participants: [String]!
        relatesTo: Relation!
        tags: [String]!
        closedAt: String
        closedBy: String
        createdAt: String!
        updatedAt: String
    }

    type Query {
        conversation(id: String): Conversation
        conversations(userId: String!, organizationId: String!, from: Int, size: Int, keyword: String): [Conversation]!
    }
`;

const resolvers = {
    Query: {
        conversation: (_: any, { id }: { id: string }) => getConversation(id),
        conversations: () => randomArray(() => getConversation(), 8, 0)
    }
};

const schema = makeExecutableSchema({
    typeDefs,
    resolvers: resolvers as any
});

export const binding = new Binding({
    schema
});
