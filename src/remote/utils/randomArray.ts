import * as faker from "faker";

export const randomArray = (generator: (index: number) => any, max = 20, min = 0) =>
    Array.from(Array(faker.random.number({ min, max, precision: 1 }))).map((v: any, i: number) => generator(i));
