import { GraphQLResolveInfo } from "graphql";
import { and } from "graphql-shield";
import gql from "graphql-tag";
import { Context } from "../../context";
import { hasRoles } from "../../rules/hasRoles";
import { isAuthenticated } from "../../rules/isAuthenticated";
import { PromiseType } from "../../types";

export const typeDefs = gql`
    extend type MessengerConversation {
        participantUsers: [User]!
    }
`;

export const resolvers = {
    MessengerConversation: {
        participantUsers: {
            fragment: `... on Conversation { participants }`,
            resolve: (
                { participants }: PromiseType<Context["models"]["conversation"]["getConversation"]>,
                args: any,
                context: Context,
                info: GraphQLResolveInfo
            ) =>
                Promise.all(
                    participants.map((id: string) => context.models.user.getUser(id, context.jwt.organizationId, info))
                )
        }
    }
};

export const permissions = {
    MessengerConversation: {
        participantUsers: and(isAuthenticated, hasRoles(["read"]))
    }
};
