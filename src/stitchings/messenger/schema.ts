import { merge } from "lodash";
import { binding } from "../../remote/messenger";
import { standardiseSchema } from "../utils/standardiseSchema";
import * as participantUsers from "./participantUsers";

const schema = standardiseSchema(binding.schema, "Messenger");

export const resolvers = merge({}, participantUsers.resolvers);

export const schemas = [schema, participantUsers.typeDefs];

export const permissions = merge({}, participantUsers.permissions);
