import { GraphQLSchema } from "graphql";
import { RenameTypes, transformSchema } from "graphql-tools";

// We do a little magic here to make sure we don't get horrible prefix duplications in the type itself
// such as `UserUser` and `MessengerMessenger`, should the type be the name as the prefix
const typePrefixFactory = (prefix: string) => (name: string) => (prefix === name ? name : `${prefix}${name}`);

export const standardiseSchema = (schema: GraphQLSchema, prefix: string) =>
    transformSchema(schema, [new RenameTypes(typePrefixFactory(prefix))]);
