import { mergeSchemas } from "graphql-tools";
import { GraphQLServer } from "graphql-yoga";
import { merge } from "lodash";
import { context } from "../context";
import * as messenger from "./messenger/schema";
import * as user from "./user/schema";
import { generateShield } from "../utils/generateShield";

const schemas = [...messenger.schemas, ...user.schemas];

const resolvers = merge({}, messenger.resolvers, user.resolvers);

const schema = mergeSchemas({
    schemas,
    resolvers
});

const shield = generateShield(merge({}, messenger.permissions, user.permissions));

export const server = new GraphQLServer({
    schema,
    context,
    middlewares: [shield]
});
