import { GraphQLResolveInfo } from "graphql";
import { addFragmentToInfo } from "graphql-binding";
import { and } from "graphql-shield";
import gql from "graphql-tag";
import { Context } from "../../context";
import { hasRoles } from "../../rules/hasRoles";
import { isAuthenticated } from "../../rules/isAuthenticated";
import { PromiseType } from "../../types";

export const typeDefs = gql`
    extend type User {
        conversations: [MessengerConversation]!
    }
`;

export const resolvers = {
    User: {
        conversations: {
            fragment: `... on User { id }`,
            resolve: (
                { id }: PromiseType<Context["models"]["user"]["getUser"]>,
                args: any,
                context: Context,
                info: GraphQLResolveInfo
            ) =>
                context.models.conversation.getUserConversations(
                    id,
                    context.jwt.organizationId,
                    addFragmentToInfo(
                        info,
                        `fragment __ConversationsParticipants on MessengerConversation {
                            participants
                        }`
                    )
                )
        }
    }
};

export const permissions = {
    User: {
        conversations: and(isAuthenticated, hasRoles(["read"]))
    }
};
