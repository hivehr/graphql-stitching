import { merge } from "lodash";
import { binding } from "../../remote/user";
import { standardiseSchema } from "../utils/standardiseSchema";
import * as conversations from "./conversations";

const schema = standardiseSchema(binding.schema, "User");

export const resolvers = merge({}, conversations.resolvers);

export const schemas = [schema, conversations.typeDefs];

export const permissions = merge({}, conversations.permissions);
