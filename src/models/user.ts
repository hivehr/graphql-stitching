import { GraphQLResolveInfo } from "graphql";
import { binding as userBinding } from "../remote/user";

export const getUser = () => () => (id: string, organizationId: string, info: GraphQLResolveInfo | string) =>
    userBinding.query.user({ id, organizationId }, info);
