import { mapValues } from "lodash";
import { MapReturnTypes } from "../types";
import * as conversation from "./conversation";
import * as user from "./user";

// Resolver factories are similar to redux middleware, which have two layers of thunks
// (dataLayer: DataLayer) => (context: ContextParameters) => (...args: any[]) => any
const modelFactories = {
    conversation,
    user
};

type MappedResolvers<O> = { [K in keyof O]: MapReturnTypes<MapReturnTypes<O[K]>> };

// This type is a map of the final resolver methods, without the dataLayer and user
// thunk parts. Useful for typing the dataLayer in the context. See context.ts
export type Models = MappedResolvers<typeof modelFactories>;

// This following code binds the initial dataLayer thunk, returning the user
// thunk part, which is bound within each request in the context.
export const models = mapValues(modelFactories, category =>
    mapValues(category, (resolver: any) => resolver(models))
) as Models;
