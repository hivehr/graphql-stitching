import { GraphQLResolveInfo } from "graphql";
import { binding as messengerBinding } from "../remote/messenger";

export const getConversation = () => () => (id: string, organizationId: string, info: GraphQLResolveInfo | string) =>
    messengerBinding.query.conversation({ id, organizationId }, info);

export const getUserConversations = () => () => (
    userId: string,
    organizationId: string,
    info: GraphQLResolveInfo | string
) => messengerBinding.query.conversations({ userId, organizationId }, info);
